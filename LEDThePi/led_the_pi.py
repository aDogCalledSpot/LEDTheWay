import time
import serial
import struct
import access_data
import json
import requests

# TODO: lightelligence function/class declarations

LEDStrip = {"WW": 1,
            "EE": 2,
            "SW": 3,
            "NW": 4,
            "SS": 5,
            "NN": 6,
            "NE": 7,
            "SE": 8}

LEDMode = {"ON": 0, "WAVE": 1}
LEDDirection = {"DOWN_RIGHT": 0, "UP_LEFT": 1}
LEDColor = {"OFF": 0,
            "RED": 1,
            "GREEN": 2,
            "BLUE": 3,
            "WHITE": 4}
RoomID = {0: "exit", 1: "reception", 2: "toilet", 3: "conference"}


def arduino_send(path, color):
    port = serial.Serial('/dev/ttyACM0', 9600)
    for (node, direction) in path:
        string = struct.pack('<BBBB', node, LEDMode["WAVE"], direction, color)
        port.write(string)
    port.close()


class Graph:
    internal = {}
    map = {
        ('A', 'B'): (LEDStrip["NW"], LEDDirection["DOWN_RIGHT"]),
        ('B', 'A'): (LEDStrip["NW"], LEDDirection["UP_LEFT"]),
        ('B', 'C'): (LEDStrip["SW"], LEDDirection["DOWN_RIGHT"]),
        ('B', 'E'): (LEDStrip["WW"], LEDDirection["DOWN_RIGHT"]),
        ('C', 'B'): (LEDStrip["SW"], LEDDirection["UP_LEFT"]),
        ('D', 'E'): (LEDStrip["NN"], LEDDirection["DOWN_RIGHT"]),
        ('E', 'B'): (LEDStrip["WW"], LEDDirection["UP_LEFT"]),
        ('E', 'D'): (LEDStrip["NN"], LEDDirection["UP_LEFT"]),
        ('E', 'F'): (LEDStrip["SS"], LEDDirection["DOWN_RIGHT"]),
        ('E', 'H'): (LEDStrip["EE"], LEDDirection["DOWN_RIGHT"]),
        ('F', 'E'): (LEDStrip["SS"], LEDDirection["UP_LEFT"]),
        ('G', 'H'): (LEDStrip["NE"], LEDDirection["DOWN_RIGHT"]),
        ('H', 'E'): (LEDStrip["EE"], LEDDirection["UP_LEFT"]),
        ('H', 'G'): (LEDStrip["NE"], LEDDirection["UP_LEFT"]),
        ('H', 'I'): (LEDStrip["SE"], LEDDirection["DOWN_RIGHT"]),
        ('I', 'H'): (LEDStrip["SE"], LEDDirection["UP_LEFT"]),
    }

    traits = {
        'A': ["toilet"],
        'B': [],
        'C': ["reception"],
        'D': [],
        'E': [],
        'F': ["conference"],
        'G': [],
        'H': ["toilet"],
        'I': ["exit"]
    }

    def __init__(self, graph):
        self.internal = graph

    def __bfs_in(self, start_node, end_trait, path):
        path = path + [start_node]
        if end_trait in self.traits[start_node]:
            return path

        for node in self.internal[start_node]:
            if node not in path:
                tmp = self.__bfs_in(node, end_trait, path)
                if tmp is not None:
                    return tmp

        return None

    def bfs(self, start_node, end_trait):
        path = self.__bfs_in(start_node, end_trait, [])
        res = []
        for i, node in enumerate(path):
            if end_trait in self.traits[node]:
                return res
            res.append(self.map[(node, path[i + 1])])
        return res


def wait_on_alexa():
    device_url = "https://api.preview.oltd.de/v1/devices/" + access_data.controller_00_id
    rest_response = requests.get(device_url, headers=access_data.headers)
    out = json.loads(rest_response.text)

    if out['data']['configuration']['id'] == 0:
        return -1

    return out['data']['configuration']['dstID']


# =======main=======
color = 1
while True:
    color = 1 + ((color + 1) % 3)
    while True:
        dst = wait_on_alexa()
        if dst != -1:
            break
        time.sleep(1.5)

    message = '{ "configuration": { "dstID": ' + str(0) + ',\
                "color": ' + str(color) + ', "id": ' + str(0) + '}}'
    url = "https://api.preview.oltd.de/v1/devices/" + access_data.controller_00_id
    response = requests.patch(url, headers=access_data.headers, data=message)

    graph = Graph({'A': ['B'],
                   'B': ['A', 'C', 'E'],
                   'C': ['B'],
                   'D': ['E'],
                   'E': ['B', 'D', 'F', 'H'],
                   'F': ['E'],
                   'G': ['H'],
                   'H': ['G', 'E', 'I'],
                   'I': ['H']
                   })

    # print(graph.bfs('G', RoomID[dst]))
    arduino_send(graph.bfs('G', RoomID[dst]), LEDColor["RED"])
