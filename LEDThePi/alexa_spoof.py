import access_data
import requests
import json
import time

LEDColor = {1: "red",
            2: "green",
            3: "blue"}

# Change values
cmd_id = 1
places = ["Exit", "Reception", "Toilet", "Conference Room"]
dst = input("Where to?\nDestination: ")
while not dst in places:
    dst = input("Invalid destination. Try again.\nDestination: ")

message = '{ "configuration": { "dstID": ' + str(places.index(dst)) + ',\
    "color": ' + str(0) + ', "id": ' + str(cmd_id) + '}}'

url = "https://api.preview.oltd.de/v1/devices/" + access_data.controller_00_id
response = requests.patch(url, headers=access_data.headers, data=message)

time.sleep(2)

response = requests.get(url, headers=access_data.headers)
print(response.text)
out = json.loads(response.text)
color = out['data']['configuration']['color']
print("Follow the color " + LEDColor[color])
