import requests
import json
import access_data

url = "https://api.preview.oltd.de/v1/device-types"
data = { "name": "Controller",
         "manufacturer": "LEDTheWay",
         "model": "Heidi Klum",
         "description": "Simple node with destination and color",
         "schema": {
             "configuration": {
                 "dstID": {
                     "type": "number"
                 },
                 "lightColor": {
                     "type": "object",
                     "properties": {
                         "r": { "type": "number" },
                         "g": { "type": "number" },
                         "b": { "type": "number" }
                     }
                 }
             },
             "attributes": {}
         }
     }

response = requests.post(url, json = data, headers = access_data.headers)
print(response.text)