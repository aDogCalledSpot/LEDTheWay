import requests
import json
import access_data

url = "https://api.preview.oltd.de/v1/device-types/" + access_data.controller_device_type_id
body = {
    "schema": {
      'configuration': {
          'dstID': {
              "type": "number"
           },
          'color': {
              "type": "number"
           },
          'id': {
              "type": "number"
          }
      }
    }
}

response = requests.patch(url, json = body, headers = access_data.headers)

print(response.text)
file = open("LEDThePi/device_response.json", "w")
file.write(response.text)