import requests
import json
import access_data

url = "https://api.preview.oltd.de/v1/devices"
body = {
  'info': {
    'name': 'Controller_00',
    'deviceTypeId': access_data.controller_device_type_id,
    'description': 'First Test node',
    'tags': [
      'controller'
    ]
  },
  'attributes': {
      'dstID': 00,
      'color': 0
  }
}

response = requests.post(url, json = body, headers = access_data.headers)

file = open("LEDThePi/device_response.json", "w")
file.write(response.text)