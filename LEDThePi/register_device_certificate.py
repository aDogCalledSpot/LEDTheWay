import requests
import json
import access_data

url = "https://api.preview.oltd.de/v1/devices/" + access_data.controller_00_id + "/certificates"
body = {
  'cert': access_data.controller_00_cert,
  'status': 'valid'
}

response = requests.post(url, json = body, headers = access_data.headers)

file = open("LEDThePi/cert_register_response.json","w")
file.write(response.text)